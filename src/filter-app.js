const { _, result } = require('lodash')
const path = require('path')
const prompt = require('prompt-sync')()
const winston = require('winston')

const logger = winston.createLogger({
	transports: [
		new winston.transports.Console(),
		new winston.transports.File({ filename: './log/output.log' }),
	],
})

function isInRange(value, range) {
	return value >= range[0] && value <= range[1]
}

function quartiles(arr, q) {
	// sort array asc
	const sorted = arr.sort(function (a, b) {
		return a - b
	})

	// find position
	const pos = (sorted.length - 1) * (q / 100)

	const base = Math.floor(pos)

	const rest = pos - base

	logger.info(sorted)
	logger.info(pos)

	if (sorted[base + 1] !== undefined) {
		return sorted[base] + rest * (sorted[base + 1] - sorted[base])
	} else {
		return sorted[base]
	}
}

let myRange = []

dataFileName = prompt('Please input your data file name : ')
// Load raw data file
const rawData = require(path.join(process.cwd(), 'data', dataFileName))

// Get start time and end time
myRange[0] = prompt('Please input start time (HH:MM:ss): ')
myRange[1] = prompt('Please input end time (HH:MM:ss): ')

// Filter only point and http_req_duration
let filterPoint = _.filter(rawData, {
	metric: 'http_req_duration',
	type: 'Point',
})

logger.info('-------------------------- POINT --------------------------')

logger.info(filterPoint)

let groups = _.uniq(_.map(filterPoint, 'data.tags.group'))

logger.info(groups)

let results = []
let group

let filterGroup, timeGroup, avg, min, max, p90
for (let gitem of groups) {
	group = {}
	group.groupName = gitem
	logger.info(
		'--------------------------' + gitem + '--------------------------'
	)
	filterGroup = _.filter(filterPoint, item => item.data.tags.group === gitem)
	logger.info(filterGroup)

	// Filter time in range
	logger.info('+++ TIME IN RANGE +++')
	timeGroup = _.filter(filterGroup, function (filterGroup) {
		return isInRange(filterGroup.data.time.substring(11, 19), myRange)
	})
	logger.info(timeGroup)

	// Get all values in group
	logger.info('+++ Values +++')
	valueGroup = _.map(timeGroup, 'data.value')
	logger.info(valueGroup)

	// Calculate avg
	logger.info('+++ Avg +++')
	avg = _.mean(valueGroup)
	logger.info(avg)
	group.avg = avg

	// Calculate min
	logger.info('+++ Min +++')
	min = _.min(valueGroup)
	logger.info(min)
	group.min = min

	// Calculate max
	logger.info('+++ Max +++')
	max = _.max(valueGroup)
	logger.info(max)
	group.max = max

	// percentile 90
	logger.info('+++ Percentile 90 +++')
	p90 = quartiles(valueGroup, 90)
	logger.info(p90)
	group.p90 = p90

	results.push(group)
}

// RESULT
logger.info('-------------------------- RESULT --------------------------')
logger.info(results)
